"""Read file and print contents"""
# Built-ins
import argparse
import sys

# PIP Installs

# Local modules

def read_my_file(filename):
    """Reads and prints contents of a file."""
    try:
        with open(filename, 'r') as f:
            file_contents = f.read()
            print("Reading file:\n====================\n{0}\n".format(filename))
            print("Contents:\n====================\n{0}".format(file_contents))
    except IOError as e:
        print("Error!! Sorry could not find file: {0}".format(filename))


def main():
    """contains our argument parser."""
    # create the argparse object
    p = argparse.ArgumentParser(description="Simple Argument Parser Example", prog='file_reader')
    # add arguments to the arg parse object
    p.add_argument('--filename', dest='filename', help='Full file path to read from', default=None)
    # Parse the arguments passed
    args = p.parse_args()
    if args.filename:
        # pass the filename from the line arguments to the function we have specified.
        read_my_file(args.filename)
main()
