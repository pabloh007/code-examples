"""Read file and print contents"""
# Built-ins
import argparse
import sys

# PIP Installs

# Local modules
def read_my_file(filename):
    """read my file text"""
    try:
        with open(filename, 'r') as f:
            file_contents = f.read()
            if len(file_contents) > 0:
                print(file_contents)
                names = file_contents.split('\n')
                print(names)
                # Loop through each name and print it out for the user
                for name in names:
                    print('Hello ' + name) 
            else:
                print('Nothing was in the file name you provided')
    except IOError as file_not_found:
        print("The file you passed is not found")
def main():
    """contains our argument parser."""
    # create the argparse object
    p = argparse.ArgumentParser(description="Simple Argument Parser Example", prog='file_reader')
    # add arguments to the arg parse object
    p.add_argument('--filename', dest='filename', help='Full file path to read from', default=None)
    # Parse the arguments passed
    args = p.parse_args()
    if args.filename:
        # pass the filename from the line arguments to the function we have specified.
        read_my_file(args.filename)
main()
